![Build Status](https://git.beagleboard.org/gsoc/gsoc.beagleboard.io/badges/main/pipeline.svg)

[gsoc.beagleboard.io](https://gsoc.beagleboard.io)

Status updates for BeagleBoard.org's Google Summer of Code program.

Primary resources:

* [Forum](https://bbb.io/gsocml)
* [Live chat](https://bbb.io/gsocchat)
* [Project documentation template](https://gsoc.beagleboard.io/project-docs-template)

2022 contributor blog sites:

* [Greybus for Zephyr](https://gsoc.beagleboard.io/greybus-for-zephyr)
* [SimpPRU](https://krishna-13-cyber.github.io/gsoc/)
* [BB-config improvements & GPIO Benchmarking](https://gsoc.beagleboard.io/bb-config/)
* [Building Bela Images](https://krvprashanth.in/gsoc2022/)
