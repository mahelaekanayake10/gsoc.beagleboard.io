:orphan:

.. _gsoc-old-ideas:

Old GSoC Ideas
##############

.. toctree::

    automation-and-industrial-io
    beaglebone-audio-applications
    beagleconnect-technology
    bela-applications
    deep-learning
    fpga-projects
    linux-kernel-improvements
    other-projects
    rtos-microkernel-improvements
    security-and-privacy
    soft-peripherals-using-co-processors

